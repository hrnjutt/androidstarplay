package com.starplay.starplaydatamanger.dataModels;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class MediaMovieDataModel extends SearchBaseDataModel{

    String title = "";
    String original_title = "";
    boolean adult = false;
    String release_date = "";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
}
