package com.starplay.starplaydatamanger.networkManger;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.starplay.starplaydatamanger.dataManger.SearchDataManger;
import com.starplay.starplaydatamanger.interfaces.SearchQueryNetworkCallBack;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class NetwrokCallManger {

    private String API_KEY = "3d0cda4466f269e793e9283f6ce0b75e";

    public void getDataFromNetworkByQuery(String query, final SearchQueryNetworkCallBack searchQueryNetworkCallBack){

        RequestParams requestParams = new RequestParams();
        requestParams.add("api_key",API_KEY);
        requestParams.add("query",query);
        requestParams.add("page","1");

        AsyncHttpClient client = new AsyncHttpClient();
        client.setURLEncodingEnabled(true);
        client.get("https://api.themoviedb.org/3/search/multi?", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    String str_response = new String(response, "UTF-8");
                    Log.d("Response",str_response);
                    updateDataMangerAndSendResult(str_response,searchQueryNetworkCallBack);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                if(searchQueryNetworkCallBack!=null) {
                    searchQueryNetworkCallBack.onFailed("Something went wrong, Please try again");
                }
            }

            @Override
            public void onRetry(int retryNo) {
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }


    private void updateDataMangerAndSendResult(String response,SearchQueryNetworkCallBack searchQueryNetworkCallBack){
        SearchDataManger searchDataManger = new SearchDataManger();
        searchDataManger.updateDataMangerWithResult(response);
        if(searchQueryNetworkCallBack!=null){
            searchQueryNetworkCallBack.onSuccess(searchDataManger);
        }


    }

}
