package com.starplay.starplaydatamanger.dataManger;

import com.google.gson.Gson;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class SearchDataManger {

    ArrayList<MediaTVDataModel> mediaTVDataModels = null;
    ArrayList<MediaMovieDataModel> mediaMovieDataModels = null;

    ArrayList<Object> mediaTypeObjesctsArray = new ArrayList<>();

    public void updateDataMangerWithResult(String result){
        try{
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject(result);
            if(jsonObject!=null && jsonObject.has("results") && !jsonObject.isNull("results")){
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                for(int index = 0; index < jsonArray.length() ; index++ ){
                    JSONObject _jsonObject1 = jsonArray.getJSONObject(index);
                    if(_jsonObject1!=null && _jsonObject1.has("media_type") && !_jsonObject1.isNull("media_type")){
                        if(_jsonObject1.getString("media_type").equalsIgnoreCase("tv")){
                            MediaTVDataModel mediaTVDataModel = gson.fromJson(_jsonObject1.toString(),MediaTVDataModel.class);
                            if(mediaTVDataModels==null){
                                mediaTVDataModels = new ArrayList<>();
                            }
                            mediaTVDataModels.add(mediaTVDataModel);
                        }else if(_jsonObject1.getString("media_type").equalsIgnoreCase("movie")){
                            MediaMovieDataModel mediaMovieDataModel = gson.fromJson(_jsonObject1.toString(),MediaMovieDataModel.class);
                            if(mediaMovieDataModels==null){
                                mediaMovieDataModels = new ArrayList<>();
                            }
                            mediaMovieDataModels.add(mediaMovieDataModel);
                        }
                    }
                }

                if(mediaTVDataModels!=null){
                    mediaTypeObjesctsArray.add(mediaTVDataModels);
                }
                if(mediaMovieDataModels!=null){
                    mediaTypeObjesctsArray.add(mediaMovieDataModels);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public ArrayList<Object> getMediaTypeObjesctsArray() {
        return mediaTypeObjesctsArray;
    }
}
