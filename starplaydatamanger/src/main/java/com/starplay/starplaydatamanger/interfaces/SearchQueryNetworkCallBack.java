package com.starplay.starplaydatamanger.interfaces;

import com.starplay.starplaydatamanger.dataManger.SearchDataManger;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public interface SearchQueryNetworkCallBack {
    void onSuccess(SearchDataManger searchDataManger);
    void onFailed(String msg);
}
