package com.starplay.starplaycodeexercise.interfaces;

import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public interface RecylerItemClickCallback {
    void itemClickedMediaTVDataModel(MediaTVDataModel mediaTVDataModel);
    void itemClickedMediaMovieDataModel(MediaMovieDataModel movieDataModel);
}
