package com.starplay.starplaycodeexercise.fragments.baseSearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.BaseSearchMoviesRVHolder;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.BaseSearchTVSRVHolder;
import com.starplay.starplaycodeexercise.interfaces.RecylerItemClickCallback;
import com.starplay.starplaydatamanger.dataManger.SearchDataManger;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

import java.util.ArrayList;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class BaseSearchRVAdapter extends RecyclerView.Adapter {

    private final int MediaTVDataModelType = 1;
    private final int MediaMovieDataModelType = 2;
    private SearchDataManger searchDataManger = null;
    private RecylerItemClickCallback recylerItemClickCallback=null;
    public BaseSearchRVAdapter(){

    }

    public BaseSearchRVAdapter(SearchDataManger _SearchDataManger,RecylerItemClickCallback _recylerItemClickCallback){
        searchDataManger = _SearchDataManger;
        recylerItemClickCallback =  _recylerItemClickCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_base_search_rvholder, parent,false);
        if(viewType == MediaTVDataModelType) {
            BaseSearchTVSRVHolder baseSearchTVSRVHolder = new BaseSearchTVSRVHolder(view,recylerItemClickCallback);
            return baseSearchTVSRVHolder;
        }else{
            BaseSearchMoviesRVHolder baseSearchMoviesRVHolder = new BaseSearchMoviesRVHolder(view,recylerItemClickCallback);
            return baseSearchMoviesRVHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        if(viewType == MediaTVDataModelType) {
            BaseSearchTVSRVHolder baseSearchTVSRVHolder = (BaseSearchTVSRVHolder) holder;
            baseSearchTVSRVHolder.updateView("TV Series", (ArrayList<MediaTVDataModel>) searchDataManger.getMediaTypeObjesctsArray().get(position));

        }else{
            BaseSearchMoviesRVHolder searchMoviesRVHolder = (BaseSearchMoviesRVHolder) holder;
            searchMoviesRVHolder.updateView("Movies", (ArrayList<MediaMovieDataModel>) searchDataManger.getMediaTypeObjesctsArray().get(position));

        }
    }

    @Override
    public int getItemViewType(int position) {
        ArrayList<Object> objects = (ArrayList<Object>) searchDataManger.getMediaTypeObjesctsArray().get(position);
        if (objects.size()>0 && objects.get(0) instanceof MediaTVDataModel) {
            return MediaTVDataModelType;
        }else if (objects.size()>0 && objects.get(0) instanceof MediaMovieDataModel){
            return MediaMovieDataModelType;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        if(searchDataManger!=null) {
            return searchDataManger.getMediaTypeObjesctsArray().size();
        }else {
            return 0;
        }
    }
}
