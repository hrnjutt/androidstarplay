package com.starplay.starplaycodeexercise.fragments.baseSearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.BaseSearchMoviesRVHolder;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.BaseSearchTVSRVHolder;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.subItems.BaseSearchMoviesItemRVHolder;
import com.starplay.starplaydatamanger.dataManger.SearchDataManger;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

import java.util.ArrayList;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class BaseSearchMoviesItemRVAdapter extends RecyclerView.Adapter {

    private ArrayList<MediaMovieDataModel> mediaMovieDataModels  = null;
    public BaseSearchMoviesItemRVAdapter(){

    }

    public BaseSearchMoviesItemRVAdapter(ArrayList<MediaMovieDataModel> _mediaMovieDataModels ){
        mediaMovieDataModels = _mediaMovieDataModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media_type_viewholder, parent,false);
        BaseSearchMoviesItemRVHolder baseSearchMoviesItemRVHolder = new BaseSearchMoviesItemRVHolder(view);
        return baseSearchMoviesItemRVHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BaseSearchMoviesItemRVHolder baseSearchMoviesItemRVHolder = (BaseSearchMoviesItemRVHolder) holder;
        baseSearchMoviesItemRVHolder.updateView(mediaMovieDataModels.get(position));

    }

    @Override
    public int getItemCount() {
        if(mediaMovieDataModels!=null) {
            return mediaMovieDataModels.size();
        }else {
            return 0;
        }
    }
}
