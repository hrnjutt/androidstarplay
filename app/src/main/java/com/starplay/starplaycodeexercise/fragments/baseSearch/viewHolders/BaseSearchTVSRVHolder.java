package com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.baseSearch.adapter.BaseSearchTVSItemRVAdapter;
import com.starplay.starplaycodeexercise.interfaces.RecylerItemClickCallback;
import com.starplay.starplaycodeexercise.utils.RecyclerItemClickListener;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

import java.util.ArrayList;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class BaseSearchTVSRVHolder extends RecyclerView.ViewHolder {
    RecylerItemClickCallback recylerItemClickCallback;
    public BaseSearchTVSRVHolder(View itemView , RecylerItemClickCallback _recylerItemClickCallback) {
        super(itemView);
        recylerItemClickCallback = _recylerItemClickCallback;
    }

    public void updateView(String title, final ArrayList<MediaTVDataModel> mediaTVDataModels){
        TextView mediaTypeTitle = itemView.findViewById(R.id.mediaTypeTitle);
        mediaTypeTitle.setText(title);

        RecyclerView recyclerView = itemView.findViewById(R.id.searchCerosalResultRV);
        BaseSearchTVSItemRVAdapter baseSearchTVSItemRVAdapter = new BaseSearchTVSItemRVAdapter(mediaTVDataModels);
        LinearLayoutManager layoutManager = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(baseSearchTVSItemRVAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(itemView.getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        if(recylerItemClickCallback!=null){
                            recylerItemClickCallback.itemClickedMediaTVDataModel(mediaTVDataModels.get(position));
                        }
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }
}
