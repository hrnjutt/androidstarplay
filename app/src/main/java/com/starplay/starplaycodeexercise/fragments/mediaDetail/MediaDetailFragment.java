package com.starplay.starplaycodeexercise.fragments.mediaDetail;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.videoPlayer.VideoPlayerFragment;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MediaDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MediaDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    MediaTVDataModel mediaTVDataModel = null;
    MediaMovieDataModel movieDataModel = null;


    public MediaDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MediaDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MediaDetailFragment newInstance(MediaTVDataModel mediaTVDataModel) {
        MediaDetailFragment fragment = new MediaDetailFragment();
        fragment.mediaTVDataModel = mediaTVDataModel;
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public static MediaDetailFragment newInstance(MediaMovieDataModel movieDataModel) {
        MediaDetailFragment fragment = new MediaDetailFragment();
        fragment.movieDataModel = movieDataModel;
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_media_detail, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.updateView();
    }

    private void updateView(){
        ImageView bannerIV = getView().findViewById(R.id.bannerIV);
        ImageView bannerIVOverlay = getView().findViewById(R.id.bannerIVOverlay);
        TextView titleTV = getView().findViewById(R.id.titleTV);
        TextView descTV = getView().findViewById(R.id.descTV);

        if(mediaTVDataModel!=null){
            titleTV.setText(mediaTVDataModel.getOriginal_name());
            descTV.setText(mediaTVDataModel.getOverview());
            Picasso.get().load("http://image.tmdb.org/t/p/w1280"+mediaTVDataModel.getBackdrop_path()).placeholder(R.drawable.placeholder).into(bannerIV);

        }else{
            titleTV.setText(movieDataModel.getOriginal_title());
            descTV.setText(movieDataModel.getOverview());
            Picasso.get().load("http://image.tmdb.org/t/p/w1280"+movieDataModel.getBackdrop_path()).placeholder(R.drawable.placeholder).into(bannerIV);

        }
        bannerIVOverlay.setVisibility(View.VISIBLE);
        bannerIVOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String backDropUrl = "";
                if(mediaTVDataModel!=null){
                    backDropUrl = mediaTVDataModel.getBackdrop_path();
                }else{
                    backDropUrl = movieDataModel.getBackdrop_path();

                }
                VideoPlayerFragment videoPlayerFragment = VideoPlayerFragment.newInstance(backDropUrl);
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.baseFullScreenFrame,videoPlayerFragment).addToBackStack(videoPlayerFragment.getClass().getSimpleName()).commit();

            }
        });
    }
}
