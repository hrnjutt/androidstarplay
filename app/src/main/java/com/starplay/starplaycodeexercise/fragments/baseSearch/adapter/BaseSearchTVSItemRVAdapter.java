package com.starplay.starplaycodeexercise.fragments.baseSearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.BaseSearchMoviesRVHolder;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.BaseSearchTVSRVHolder;
import com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.subItems.BaseSearchTVSItemRVHolder;
import com.starplay.starplaydatamanger.dataManger.SearchDataManger;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

import java.util.ArrayList;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class BaseSearchTVSItemRVAdapter extends RecyclerView.Adapter {

    private ArrayList<MediaTVDataModel> mediaTVDataModels = null;
    public BaseSearchTVSItemRVAdapter(){

    }

    public BaseSearchTVSItemRVAdapter(ArrayList<MediaTVDataModel> _mediaTVDataModels){
        mediaTVDataModels = _mediaTVDataModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media_type_viewholder, parent,false);
        BaseSearchTVSItemRVHolder baseSearchTVSItemRVHolder = new BaseSearchTVSItemRVHolder(view);
        return baseSearchTVSItemRVHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BaseSearchTVSItemRVHolder baseSearchTVSItemRVHolder = (BaseSearchTVSItemRVHolder) holder;
        baseSearchTVSItemRVHolder.updateView(mediaTVDataModels.get(position));
    }

    @Override
    public int getItemCount() {
        if(mediaTVDataModels!=null) {
            return mediaTVDataModels.size();
        }else {
            return 0;
        }
    }
}
