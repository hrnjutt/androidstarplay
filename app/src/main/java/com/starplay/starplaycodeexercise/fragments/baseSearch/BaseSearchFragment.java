package com.starplay.starplaycodeexercise.fragments.baseSearch;

import android.app.ProgressDialog;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.baseSearch.adapter.BaseSearchRVAdapter;
import com.starplay.starplaycodeexercise.fragments.mediaDetail.MediaDetailFragment;
import com.starplay.starplaycodeexercise.interfaces.RecylerItemClickCallback;
import com.starplay.starplaydatamanger.dataManger.SearchDataManger;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;
import com.starplay.starplaydatamanger.interfaces.SearchQueryNetworkCallBack;
import com.starplay.starplaydatamanger.networkManger.NetwrokCallManger;

/**
 * A placeholder fragment containing a simple view.
 */
public class BaseSearchFragment extends Fragment  implements SearchQueryNetworkCallBack , SearchView.OnQueryTextListener , RecylerItemClickCallback {

    private ProgressDialog dialog;

    public static BaseSearchFragment newInstace() {
        BaseSearchFragment baseSearchFragment = new BaseSearchFragment();
        return baseSearchFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.base_search_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initView();
    }

    private void initView(){
        setupSearchView((SearchView) getView().findViewById(R.id.search_bar));
        dialog = new ProgressDialog(getContext());
    }


    private void setupSearchView(SearchView mSearchView)
    {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setQueryHint("Search Here");
    }

    /**
     * Network Callbacks
     *
     * @param searchDataManger
     */
    @Override
    public void onSuccess(SearchDataManger searchDataManger) {
        BaseSearchRVAdapter baseSearchRVAdapter = new BaseSearchRVAdapter(searchDataManger,this);
        if(getView()!=null){
            RecyclerView searchResultRV = getView().findViewById(R.id.searchResultRV);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            searchResultRV.setLayoutManager(mLayoutManager);
            searchResultRV.setAdapter(baseSearchRVAdapter);
        }
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onFailed(String msg) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        Snackbar snackbar = Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    /**
     * SearchView Callbacks
     *
     * @param query
     * @return
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        if(!query.isEmpty() && query.length() >= 1){
            NetwrokCallManger netwrokCallManger = new NetwrokCallManger();
            netwrokCallManger.getDataFromNetworkByQuery(query, this);
            SearchView searchView = getView().findViewById(R.id.search_bar);
            searchView.clearFocus();
            dialog.setMessage("Searching, please wait.");
            dialog.show();
            return false;
        }
        Snackbar snackbar = Snackbar.make(getView(), "Please enter valid query", Snackbar.LENGTH_LONG);
        snackbar.show();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        return false;
    }


    /**
     * RecylerItemClickCallback
     *
     * @param mediaTVDataModel
     */
    @Override
    public void itemClickedMediaTVDataModel(MediaTVDataModel mediaTVDataModel) {
        MediaDetailFragment mediaDetailFragment = MediaDetailFragment.newInstance(mediaTVDataModel);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_holder,mediaDetailFragment).addToBackStack(mediaDetailFragment.getClass().getSimpleName()).commit();

    }

    @Override
    public void itemClickedMediaMovieDataModel(MediaMovieDataModel movieDataModel) {
        MediaDetailFragment mediaDetailFragment = MediaDetailFragment.newInstance(movieDataModel);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_holder,mediaDetailFragment).addToBackStack(mediaDetailFragment.getClass().getSimpleName()).commit();

    }
}
