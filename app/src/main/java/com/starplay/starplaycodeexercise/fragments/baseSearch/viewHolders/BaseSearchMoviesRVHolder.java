package com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaycodeexercise.fragments.baseSearch.adapter.BaseSearchMoviesItemRVAdapter;
import com.starplay.starplaycodeexercise.fragments.baseSearch.adapter.BaseSearchTVSItemRVAdapter;
import com.starplay.starplaycodeexercise.interfaces.RecylerItemClickCallback;
import com.starplay.starplaycodeexercise.utils.RecyclerItemClickListener;
import com.starplay.starplaydatamanger.dataModels.MediaMovieDataModel;

import java.util.ArrayList;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class BaseSearchMoviesRVHolder extends RecyclerView.ViewHolder {
    RecylerItemClickCallback recylerItemClickCallback;
    public BaseSearchMoviesRVHolder(View itemView,RecylerItemClickCallback _recylerItemClickCallback) {
        super(itemView);
        recylerItemClickCallback = _recylerItemClickCallback;
    }

    public void updateView(String title, final ArrayList<MediaMovieDataModel> mediaMovieDataModels ){
        TextView mediaTypeTitle = itemView.findViewById(R.id.mediaTypeTitle);
        mediaTypeTitle.setText(title);
        RecyclerView recyclerView = itemView.findViewById(R.id.searchCerosalResultRV);
        BaseSearchMoviesItemRVAdapter searchMoviesItemRVAdapter = new BaseSearchMoviesItemRVAdapter(mediaMovieDataModels);
        LinearLayoutManager layoutManager = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(searchMoviesItemRVAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(itemView.getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        if(recylerItemClickCallback!=null){
                            recylerItemClickCallback.itemClickedMediaMovieDataModel(mediaMovieDataModels.get(position));
                        }
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }
}
