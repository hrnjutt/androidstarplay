package com.starplay.starplaycodeexercise.fragments.baseSearch.viewHolders.subItems;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.starplay.starplaycodeexercise.R;
import com.starplay.starplaydatamanger.dataModels.MediaTVDataModel;

/**
 * Created by Haroon_CIG on 14/03/2018.
 */

public class BaseSearchTVSItemRVHolder extends RecyclerView.ViewHolder {
    public BaseSearchTVSItemRVHolder(View itemView) {
        super(itemView);
    }

    public void updateView(MediaTVDataModel mediaTVDataModel){
        TextView mediaTitle = itemView.findViewById(R.id.mediaTitle);
        mediaTitle.setText(mediaTVDataModel.getOriginal_name());
        mediaTitle.setSelected(true);

        ImageView mediaBanner = itemView.findViewById(R.id.mediaBanner);
        Picasso.get().load("http://image.tmdb.org/t/p/w342"+mediaTVDataModel.getPoster_path()).placeholder(R.drawable.placeholder).into(mediaBanner);

    }
}
